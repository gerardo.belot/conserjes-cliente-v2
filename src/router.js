import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'home',
      component: () => import( /* webpackChunkName: "login" */ './views/Login.vue'),
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import( /* webpackChunkName: "dashboard" */ './views/Dashboard.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/task/transporte/:id',
      name: 'TaskTransExternos',
      component: () => import( /* webpackChunkName: "transporte" */ './views/task/TaskTransExternos.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/task/clinica-externa/:id',
      name: 'TaskClinicaExterna',
      component: () => import( /* webpackChunkName: "TaskClinicaExterna" */ './views/task/TaskClinicaExterna.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/task/clinica-interna/:id',
      name: 'TaskClinicaInterna',
      component: () => import( /* webpackChunkName: "TaskClinicaInterna" */ './views/task/TaskClinicaInterna.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/task/domicilio/:id',
      name: 'TaskDomicilio',
      component: () => import( /* webpackChunkName: "TaskDomicilio" */ './views/task/TaskDomicilio.vue'),
      meta: {
        requiresAuth: true
      }
    },
    //congeladas
    {
      path: '/task/congeladas/:id',
      name: 'TaskCongeladas',
      component: () => import( /* webpackChunkName: "TaskCongeladas" */ './views/task/TaskCongeladas.vue'),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/task/terminaciones',
      name: 'taskTermination',
      component: () => import( /* webpackChunkName: "TaskClinicaInterna" */ './views/task/TaskTermination'),
      meta: {
        requiresAuth: true
      }
    }
  ]
});

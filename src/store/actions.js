import server from '../server'
import axios from 'axios'
import getters from './getters'

export default {

  setRecepcionistasList: ({
    commit,
    getters
  }, params) => {
    return new Promise((resolve, reject) => {
      const string = `${server}/api/task/recepcionistas`
      axios.get(string, {
        headers: {
          'Authorization': getters.getAccessToken
        }
      }).then(resp => {
        commit('SET_RECEPCIONISTAS_LIST', resp.data)
        resolve(resp)
      }).catch(err => {
        reject(err)
      })
    })
  },

  setTaskCongeladasDetail: ({commit,getters}, params) => {
    return new Promise((resolve, reject) => {
      const string = `${server}/api/task/congeladas/${params}`
      axios.get(string, {
        headers: {
          'Authorization': getters.getAccessToken
        }
      }).then(resp => {
          resolve(resp)
          commit('SET_TASK_DETAIL', resp.data)
      }).catch(err => {
          reject(err)
      })
    })
  },

  setTaskDomiciliosDetail: ({
    commit,
    getters
  }, params) => {
    return new Promise((resolve, reject) => {
      const string = `${server}/api/task/domicilios/${params}`
      axios.get(string, {
        headers: {
          'Authorization': getters.getAccessToken
        }
      }).then(resp => {
        resolve(resp)
        commit('SET_TASK_DETAIL', resp.data)
      }).catch(err => {
        reject(err)
      })
    })
  },


  setTaskTransitoList: ({
    commit,
    getters
  }, params) => {
    return new Promise((resolve, reject) => {
      const string = `${server}/api/task/transitos/${params}`
      axios.get(string, {
        headers: {
          'Authorization': getters.getAccessToken
        }
      }).then(resp => {
        resolve(resp)
        commit('SET_TASK_TRANSITO_LIST', resp.data);
      }).catch(err => {
        reject(err)
      })
    })
  },

  setTaskClinicaInternasDetail: ({
    commit,
    getters
  }, param) => {
    return new Promise((resolve, reject) => {
      const string = `${server}/api/task/clinica-interna/${param}`
      axios.get(string, {
        headers: {
          'Authorization': getters.getAccessToken
        }
      }).then(resp => {
        resolve(resp)
        commit('SET_TASK_DETAIL', resp.data)
      }).catch(err => {
        reject(err)
      })
    })
  },

  setTaskClinicaExternasDetail: ({
    commit,
    getters
  }, params) => {
    return new Promise((resolve, reject) => {
      const string = `${server}/api/task/clinicas-externas/${params}`
      axios.get(string, {
        headers: {
          'Authorization': getters.getAccessToken
        }
      }).then(resp => {
        resolve(resp)
        commit('SET_TASK_DETAIL', resp.data)
      }).catch(err => {
        reject(err)
      })
    })
  },

  setTaskTransportesExternosDetail: ({
    commit,
    getters
  }, params) => {
    return new Promise((resolve, reject) => {
      const string = `${server}/api/task/transportes-externos/${params}`
      axios.get(string, {
        headers: {
          'Authorization': getters.getAccessToken
        }
      }).then(resp => {
        resolve(resp)
        commit('SET_TASK_DETAIL', resp.data)
      }).then(err => {
        reject(err)
      })
    })
  },

  setUserInfo: ({
    commit,
    getters
  }) => {
    return new Promise((resolve, reject) => {
      const string = `${server}/api/auth/user`
      axios.get(string, {
        headers: {
          'Authorization': getters.getAccessToken
        }
      }).then(resp => {
        const datas = resp.data
        commit('SET_USER', datas.user)
        commit('SET_USER_LIST', datas.task)
        resolve(resp)
      }).catch(err => {
        reject(err)
      })
    })
  },

  AUTH_REQUEST: ({
    commit,
    dispatch
  }, user) => {
    return new Promise((resolve, reject) => { // The Promise used for router redirect in login
      let string = `${server}/api/auth/login`;
      commit('AUTH_REQUEST')
      axios({
          url: string,
          data: user,
          method: 'POST'
        })
        .then(resp => {

          commit('LOGIN_SUCCESS', resp.data)
          // you have your token, now log in your user :)
          resolve(resp)
        })
        .catch(err => {
          reject(err)
        })
    })
  }
}

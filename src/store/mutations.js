export default {
  AUTH_REQUEST(state){
    state.status = 'loading'
  },
  LOGIN_SUCCESS(state, payload) {
    state.isLoggedIn = true;
    state.access_token = payload.access_token;
    state.token_type = payload.token_type;
    state.expires_at = payload.expires_at;
    state.errors = {},
    localStorage.setItem('isLoggedIn', true);
    localStorage.setItem('access_token', payload.access_token);
    localStorage.setItem('token_type', payload.access_token);
    localStorage.setItem('expires_at', payload.expires_at);
  },

  LOGOUT(state) {
    state.isLoggedIn = false;
    state.access_token = '';
    state.token_type = '';
    state.expires_at = '';
    state.user = '';
    state.userList = [];
    localStorage.removeItem('isLoggedIn');
    localStorage.removeItem('access_token');
    localStorage.removeItem('token_type');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('user');
  },

  showSnackbar(state, payload) {
    state.snackbar.text = payload.message
    state.snackbar.multiline = (payload.message.length > 50) ? true : false

    if (payload.multiline) {
      state.snackbar.multiline = payload.multiline
    }

    if (payload.timeout) {
      state.snackbar.timeout = payload.timeout
    }

    state.snackbar.visible = true
  },

  closeSnackbar(state) {
    state.snackbar.visible = false
    state.snackbar.multiline = false
    state.snackbar.timeout = 6000
    state.snackbar.text = null
  },

  SET_ERRORS(state, payload) {
    state.errors = payload;
  },

  SET_USER(state, payload) {
    state.user = payload;
    localStorage.setItem('user', JSON.stringify(payload));
  },

  SET_USER_LIST(state, payload) {
    state.userList = payload;
  },

  SET_TASK_DETAIL(state, payload) {
    state.task = payload;
  },

  SET_TASK_TRANSITO_LIST(state, payload) {
    state.taskTransitos = payload;
  },

  SET_RECEPCIONISTAS_LIST(state, payload) {
    state.recepcionistaList = payload;
  },



}

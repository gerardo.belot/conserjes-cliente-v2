import state from './states'

export default {
  getIsLogedIn(state) {
    return state.isLoggedIn;
  },
  getAccessToken(state) {
    return state.token_type + ' ' + state.access_token;
  },
  getExpires(state) {
    return state.expires_at;
  },
  getUser(state) {
    return state.user;
  },
  getUserId(state) {
    return state.user.id;
  },
  getUserList(state) {
    return state.userList;
  },
  getErrors(state) {
    return state.errors;
  },
  getTaskDetail(state) {
    return state.task;
  },
  getTaskTransitosDetail(state) {
    return state.taskTransitos;
  },
  getRecepcionistaList(state){
    return state.recepcionistaList;
  }
}

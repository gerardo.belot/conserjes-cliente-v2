export default {
  status: '',
  access_token: localStorage.getItem('access_token'),
  token_type: localStorage.getItem('token_type'),
  expires_at: localStorage.getItem('expires_at'),
  isLoggedIn: localStorage.getItem('isLoggedIn'),
  user: JSON.stringify(localStorage.getItem('user')),
  userList: [],
  errors: [],
  snackbar: {
    visible: false,
    text: null,
    timeout: 6000,
    multiline: false,
  },
  task: {
    name: '',
    address: '',
    notes: ''
  },
  taskTransitos: {},
  recepcionistaList:{},
}

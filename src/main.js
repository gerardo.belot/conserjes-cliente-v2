import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store/store'
import VueGeolocation from 'vue-browser-geolocation';

Vue.use(VueGeolocation);

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {

    if (!store.getters.getIsLogedIn) {
      next({
        path: '/'
      })
    } else {
      next()
    }
  }

  if (to.path == '/dashboard'){
    console.log(to.matched.some(record => record.meta.requiresAuth))
    console.log(store.getters.getIsLogedIn);
  }
  next()
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
